package com.example.ankit.retrofitdemo.retrofitHandler;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

public interface FileUploadService {

    //in this api "description" is the parameter name you want to send your string with
    // and "imageFile" is the parameter name you want to send your image file with
    @Multipart
    @POST("/upload")
    //here your api will come
    void upload(@Part("description") String description,
                @Part("imageFile") TypedFile file,
                Callback<Response> callback);

}
