package com.example.ankit.retrofitdemo.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.ankit.retrofitdemo.R;
import com.example.ankit.retrofitdemo.retrofitHandler.RequestDataModel;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnUpload = (Button) findViewById(R.id.btn_upload);

        if(btnUpload != null) {
            btnUpload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    uploadFile();
                }
            });
        }
    }

    private void uploadFile() {
        //get the description string. (This will be radio button text in your case)
        String descriptionString = "hello, this is description speaking";

        //get the file to upload (I am using some random file name). Somehow get the file to be uploaded (either from uri or path)
        File file = new File("abc.png");

        RequestDataModel.uploadFile(file, descriptionString);
    }

}
