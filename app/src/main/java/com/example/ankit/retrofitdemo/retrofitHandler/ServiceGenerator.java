package com.example.ankit.retrofitdemo.retrofitHandler;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class ServiceGenerator {

    public static final String API_BASE_URL = "http://your.api-base.url";

    public static <S> S createService(Class<S> serviceClass) {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(API_BASE_URL)
                .setClient(new OkClient());

        RestAdapter adapter = builder.build();

        return adapter.create(serviceClass);
    }
}
