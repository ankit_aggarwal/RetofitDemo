package com.example.ankit.retrofitdemo.retrofitHandler;

import android.util.Log;

import java.io.File;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class RequestDataModel {

    public static void uploadFile(File file, String descriptionString) {
        TypedFile fileToUpload = new TypedFile("multipart/form-data", file);

        ServiceGenerator.createService(FileUploadService.class).upload(descriptionString,
                fileToUpload, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        Log.d("upload", "success");
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d("upload failed", error.getMessage());
                    }
                });
    }
}
